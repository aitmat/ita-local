<?php

namespace App\Model\Api;


class ApiContext extends AbstractApiContext
{
    const ENDPOINT_PING = '/ping';
    const ENDPOINT_CLIENT = '/client';
    const ENDPOINT_CLIENT_ADD_SOCIAL_NETWORK = '/client/add_social_network/bind';
    const ENDPOINT_CLIENT_ENCODE_PASSWORD = '/client/encode/password';
    const ENDPOINT_CONCRETE_CLIENT = '/client/passport/{passport}/{email}';
    const ENDPOINT_CHECK_CLIENT_CREDENTIALS = '/check_client_credentials/{encodedPassword}/{email}';
    const ENDPOINT_AUTH_CLIENT_WITH_SOCIAL_NETWORK = '/client/auth_social/if_exists';
    const ENDPOINT_CONCRETE_CLIENT_BY_EMAIL = '/client/email/{email}';
    const ENDPOINT_CHECK_CLIENT_ALREADY_ATTACHED = '/client/check/already_attached/{uid}';
    const ENDPOINT_CREATE_NEW_OBJECT = '/object/new/create';
    const ENDPOINT_GET_ALL_OBJECTS = '/object/get/objects';
    const ENDPOINT_GET_BY_FILTRATION = '/object/get/filtration';
    const ENDPOINT_GET_CONCRETE_OBJECT = '/get/concrete/object';
    const ENDPOINT_BOOKING_OBJECT_ROOM = '/booking/object/room';


    /**
     * @return mixed
     * @throws ApiException
     */
    public function makePing()
    {
        return $this->makeQuery(self::ENDPOINT_PING, self::METHOD_GET);
    }

    /**
     * @param string $passport
     * @param string $email
     * @return bool
     * @throws ApiException
     */
    public function clientExists(string $passport, string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT, [
            'passport' => $passport,
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createClient(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_CLIENT, self::METHOD_POST, $data);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function addSocialNetworkClient(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_CLIENT_ADD_SOCIAL_NETWORK, self::METHOD_POST, $data);
    }

    /**
     * @param string $plainPassword
     * @param string $email
     * @return bool
     * @throws ApiException
     */
    public function checkClientCredentials(string $plainPassword, string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CHECK_CLIENT_CREDENTIALS, [
            'email' => $email,
            'encodedPassword' => $plainPassword,
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param array $data
     * @return array
     * @throws ApiException
     */
    public function authClientWithSocialNetwork(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_AUTH_CLIENT_WITH_SOCIAL_NETWORK, self::METHOD_GET, $data);
    }

    /**
     * @param string $email
     * @return array
     * @throws ApiException
     */
    public function signInClient(string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT_BY_EMAIL, [
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }

    /**
     * @param $password
     * @return mixed
     * @throws ApiException
     */
    public function passwordEncode($password)
    {

        $response = $this->makeQuery(self::ENDPOINT_CLIENT_ENCODE_PASSWORD, self::METHOD_GET, [
            'plainPassword' => $password
        ]);
        return $response['result'];
    }

    /**
     * @param $uid
     * @return mixed
     * @throws ApiException
     */
    public function checkClientAlreadyAttached($uid)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CHECK_CLIENT_ALREADY_ATTACHED, [
            'uid' => $uid,
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createNewObject(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_CREATE_NEW_OBJECT, self::METHOD_POST, $data);
    }

    /**
     * @return mixed
     * @throws ApiException
     */
    public function getAllObjects()
    {
        return $this->makeQuery(self::ENDPOINT_GET_ALL_OBJECTS, self::METHOD_GET);
    }

    /**
     * @param $data
     * @return mixed
     * @throws ApiException
     */
    public function findByFiltration($data)
    {
        return $this->makeQuery(self::ENDPOINT_GET_BY_FILTRATION, self::METHOD_GET, $data);
    }

    /**
     * @param $name
     * @return mixed
     * @throws ApiException
     */
    public function getByNameObject($name)
    {
        return $this->makeQuery(self::ENDPOINT_GET_CONCRETE_OBJECT, self::METHOD_GET, ['name' => $name]);
    }

    /**
     * @param $data
     * @return mixed
     * @throws ApiException
     */
    public function bookingRoomObject($data)
    {
        return $this->makeQuery(self::ENDPOINT_BOOKING_OBJECT_ROOM, self::METHOD_POST, $data);
    }
}