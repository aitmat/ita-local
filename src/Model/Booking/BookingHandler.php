<?php

namespace App\Model\Booking;


use App\Model\Object\BookingObject;
use App\Model\Object\Cottage;
use App\Model\Object\Pension;

class BookingHandler
{
    const PENSION = 'Pension';
    const COTTAGE = 'Cottage';
    const PENSION_NAMESPACE = 'App\Entity\Pension';
    const COTTAGE_NAMESPACE = 'App\Entity\Cottage';

    public function objectMerged($formData, BookingObject $mainData)
    {
        /** @var Pension | Cottage $formData */
        $formData
            ->setName($mainData->getName())
            ->setAmountRooms($mainData->getAmountRooms())
            ->setContactPerson($mainData->getContactPerson())
            ->setContactTelephone($mainData->getContactTelephone())
            ->setAddress($mainData->getAddress())
            ->setPricePerNight($mainData->getPricePerNight())
            ->setDiscriminator($mainData->getDiscriminator())
            ->setLandlord($mainData->getLandlord())
        ;
        return $formData;
    }

    /**
     * @param array $objects
     * @return array
     */
    public function arrayToObject(array $objects)
    {
        $array = [];
        foreach ($objects as $object){
            if ($object['class'] == self::COTTAGE_NAMESPACE){
                $cottage = new Cottage();
                $cottage
                    ->setName($object['name'])
                    ->setAmountRooms($object['amountRooms'])
                    ->setContactPerson($object['contactPerson'])
                    ->setContactTelephone($object['contactTelephone'])
                    ->setAddress($object['address'])
                    ->setPricePerNight($object['pricePerNight'])
                    ->setKitchen($object['kitchen'])
                    ->setGarden($object['garden']);
                $array[] = $cottage;
            } elseif ($object['class'] == self::PENSION_NAMESPACE){
                $pension = new Pension();
                $pension
                    ->setName($object['name'])
                    ->setAmountRooms($object['amountRooms'])
                    ->setContactPerson($object['contactPerson'])
                    ->setContactTelephone($object['contactTelephone'])
                    ->setAddress($object['address'])
                    ->setPricePerNight($object['pricePerNight'])
                    ->setCinema($object['kitchen'])
                    ->setDisco($object['garden']);
                $array[] = $pension;
            }
        }
        return $array;
    }
}
