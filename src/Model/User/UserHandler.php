<?php

namespace App\Model\User;

use App\Entity\User;

use App\Model\Api\ApiContext;
use Psr\Container\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class UserHandler
{
    const SOC_NETWORK_VKONTAKTE = "vkontakte";
    const SOC_NETWORK_FACEBOOK = "facebook";
    const SOC_NETWORK_GOOGLE = "google";
    const ROLE_LAN = 'role-1';
    const ROLE_TEN = 'role-2';
    const ROLE_LANDLORD = 'ROLE_LANDLORD';
    const ROLE_TENANT = 'ROLE_TENANT';

    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var ApiContext
     */
    private $apiContext;

    public function __construct(ContainerInterface $container, ApiContext $apiContext)
    {
        $this->container = $container;
        $this->apiContext = $apiContext;
    }

    public function encodeRole($role)
    {
        $res = null;
        if ($role == self::ROLE_LAN){
            $res =  self::ROLE_LANDLORD;
        } elseif ($role == self::ROLE_TEN) {
            $res = self::ROLE_TENANT;
        }
        return $res;
    }

    /**
     * @param array $data
     * @param bool $encodePassword
     * @return User
     * @throws \App\Model\Api\ApiException
     */
    public function createNewUser(array $data, bool $encodePassword = true) {
        $user = new User();
        $user->setEmail($data['email']);
        $user->setPassport($data['passport']);
        $user->addRole($data['role'] ?? null);
        $user->setVkId($data['vkId'] ?? null);
        $user->setFaceBookId($data['faceBookId'] ?? null);
        $user->setGoogleId($data['googleId'] ?? null);
        if($encodePassword) {
            $password = $this->apiContext->passwordEncode($data['password']);
        } else {
            $password = $data['password'];
        }

        $user->setPassword($password);

        return $user;
    }


    public function makeUserSession(User $user) {
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this
            ->container
            ->get('security.token_storage')
            ->setToken($token);
        $this
            ->container
            ->get('session')
            ->set('_security_main', serialize($token));
    }
}
