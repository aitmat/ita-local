<?php

namespace App\Model\Object;
use Symfony\Component\Validator\Constraints as Assert;

class BookingObject
{
    protected $discriminator;

    protected $name;

    protected $amountRooms;

    protected $contactPerson;

    protected $contactTelephone;

    protected $landlord;

    /**
     * @Assert\NotBlank()
     */
    protected $address;

    protected $pricePerNight;

    /**
     * @param mixed $name
     * @return BookingObject
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $amountRooms
     * @return BookingObject
     */
    public function setAmountRooms($amountRooms)
    {
        $this->amountRooms = $amountRooms;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAmountRooms()
    {
        return $this->amountRooms;
    }

    /**
     * @param mixed $contactPerson
     * @return BookingObject
     */
    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * @param mixed $contactTelephone
     * @return BookingObject
     */
    public function setContactTelephone($contactTelephone)
    {
        $this->contactTelephone = $contactTelephone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContactTelephone()
    {
        return $this->contactTelephone;
    }

    /**
     * @param mixed $address
     * @return BookingObject
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $pricePerNight
     * @return BookingObject
     */
    public function setPricePerNight($pricePerNight)
    {
        $this->pricePerNight = $pricePerNight;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPricePerNight()
    {
        return $this->pricePerNight;
    }

    /**
     * @param mixed $discriminator
     * @return BookingObject
     */
    public function setDiscriminator($discriminator)
    {
        $this->discriminator = $discriminator;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDiscriminator()
    {
        return $this->discriminator;
    }

    /**
     * @param mixed $landlord
     * @return BookingObject
     */
    public function setLandlord($landlord)
    {
        $this->landlord = $landlord;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLandlord()
    {
        return $this->landlord;
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    public function getClassName()
    {
        return (new \ReflectionClass($this))->getShortName();
    }
}
