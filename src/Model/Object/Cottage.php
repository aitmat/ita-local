<?php

namespace App\Model\Object;


class Cottage extends BookingObject
{
    private $kitchen;

    private $garden;

    /**
     * @param mixed $kitchen
     * @return Cottage
     */
    public function setKitchen($kitchen)
    {
        $this->kitchen = $kitchen;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getKitchen()
    {
        return $this->kitchen;
    }

    /**
     * @param mixed $garden
     * @return Cottage
     */
    public function setGarden($garden)
    {
        $this->garden = $garden;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGarden()
    {
        return $this->garden;
    }

    public function toArray(){
        return [
          'discriminator' => $this->discriminator,
          'name' => $this->name,
          'amountRooms' => $this->amountRooms,
          'contactPerson' => $this->contactPerson,
          'contactTelephone' => $this->contactTelephone,
          'address' => $this->address,
          'pricePerNight' => $this->pricePerNight,
          'kitchen' => $this->kitchen,
          'garden' => $this->garden,
          'landlord' => $this->landlord,
        ];
    }
}
