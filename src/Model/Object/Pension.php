<?php

namespace App\Model\Object;

class Pension extends BookingObject
{

    private $cinema;

    private $disco;

    /**
     * @param mixed $cinema
     * @return Pension
     */
    public function setCinema($cinema)
    {
        $this->cinema = $cinema;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCinema()
    {
        return $this->cinema;
    }

    /**
     * @param mixed $disco
     * @return Pension
     */
    public function setDisco($disco)
    {
        $this->disco = $disco;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDisco()
    {
        return $this->disco;
    }

    public function toArray(){
        return [
            'discriminator' => $this->discriminator,
            'name' => $this->name,
            'amountRooms' => $this->amountRooms,
            'contactPerson' => $this->contactPerson,
            'contactTelephone' => $this->contactTelephone,
            'address' => $this->address,
            'pricePerNight' => $this->pricePerNight,
            'cinema' => $this->cinema,
            'disco' => $this->disco,
            'landlord' => $this->landlord,
        ];
    }
}
