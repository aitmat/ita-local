<?php

namespace App\Controller;

use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class uloginController extends Controller
{

    const ULOGIN_DATA = 'ulogin-data';

    /**
     * @Route("/register-case", name="app-register-case")
     */
    public function registerCaseAction()
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);

        $user = new User();
        $user->setEmail($userData['email']);

        $form = $this->createForm(
            'App\Form\ULoginRegisterType', $user, ['action' => $this
                    ->get('router')
                    ->generate('app-register-case2')]
        );
        $formRole = $this->createForm('App\Form\ChoiceRoleType');
        $this->get('session')->set(self::ULOGIN_DATA, $s);

        return $this->render('registration/ul_2_state.html.twig', [
            'form' => $form->createView(),
            'formRole' => $formRole->createView(),
            'error' => null
        ]);
    }

    /**
     * @Route("/register-case2", name="app-register-case2")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @param UserRepository $repository
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function registerCase2Action(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler,
        UserRepository $repository
    )
    {
        $userData = json_decode(
            $this
                ->get('session')
                ->get(self::ULOGIN_DATA),
            true
        );

        $user = new User();

        $form = $this->createForm('App\Form\ULoginRegisterType', $user);
        $formRole = $this->createForm('App\Form\ChoiceRoleType');

        $form->handleRequest($request);
        $formRole->handleRequest($request);


        $error = null;
        if ($formRole->isSubmitted() && $formRole->isValid()) {

            try {
                if ($apiContext->clientExists($user->getPassport(), $user->getEmail())) {
                    $error = 'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.';
                } else {
                    if ($repository->findOneBySocialId($userData['uid'])){
                        $this->addFlash(
                            'error_message',
                            'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.'
                        );
                        return $this->redirectToRoute("sign-in");
                    }
                    $user->setPassword("social------------------------".time());
                    $user->setSocialId($userData['network'], $userData['uid']);
                    $data = $user->__toArray();
                    $apiContext->createClient($data);
                    $data['role'] = $userHandler->encodeRole($formRole->getData()['role']);
                    $user = $userHandler->createNewUser($data);
                    $manager->persist($user);
                    $manager->flush();

                    return $this->redirectToRoute("ping");
                }
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage() . '  |||  ' . var_export($e->getResponse(), 1);
            }
        }

        return $this->render('registration/ul_2_state.html.twig', [
            'form' => $form->createView(),
            'formRole' => $formRole->createView(),
            'error' => $error
        ]);
    }
}
