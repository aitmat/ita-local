<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\FiltrationType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\Booking\BookingHandler;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
     * @Route("/sign-up", name="app-sign-up")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return Response
     */
    public function signUpAction(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $error = null;
        $user = new User();
        $form = $this->createForm("App\Form\RegisterType", $user);
        $formRole = $this->createForm("App\Form\ChoiceRoleType");

        $form->handleRequest($request);
        $formRole->handleRequest($request);


        if ($formRole->isSubmitted() && $formRole->isValid()) {
            try {
                if ($apiContext->clientExists($user->getPassport(), $user->getEmail())) {
                    $error = 'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.';
                } else {
                    $data = [
                        'email' => $user->getEmail(),
                        'passport' => $user->getPassport(),
                        'password' => $user->getPassword(),
                        'role' => $userHandler->encodeRole($formRole->getData()['role']),
                    ];
                    $apiContext->createClient($data);
                    $user = $userHandler->createNewUser($data);
                    $manager->persist($user);
                    $manager->flush();
                    $userHandler->makeUserSession($user);
                    return $this->redirectToRoute("app_show_profile");
                }
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage().'  |||  '.var_export($e->getResponse(),1);
            }
        }

        return $this->render('sign_up.html.twig', [
            'form' => $form->createView(),
            'formRole' => $formRole->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/", name="ping")
     * @param ApiContext $apiContext
     * @param BookingHandler $bookingHandler
     * @param Request $request
     * @return Response
     * @throws ApiException
     */
    public function indexAction(ApiContext $apiContext, BookingHandler $bookingHandler, Request $request)
    {
        $error = null;
        $form = $this->createForm(FiltrationType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $data = $form->getData();
            try {
                $objectsArray = $apiContext->findByFiltration($data);
                $objects = $bookingHandler->arrayToObject($objectsArray);
                return $this->render('index.html.twig', [
                    'objects' => $objects,
                    'form' => $form->createView(),
                    'error' => $error,
                ]);
            } catch (ApiException $e) {
                $error = 'Не найден!';
            }
        }
        $objectsArray = $apiContext->getAllObjects();
        $objects = $bookingHandler->arrayToObject($objectsArray);
        return $this->render('index.html.twig', [
            'objects' => $objects,
            'form' => $form->createView(),
            'error' => $error,
        ]);
    }

    /**
     * @Route("/profile", name="profile")
     * @param ApiContext $apiContext
     * @param ObjectManager $manager
     * @return Response
     * @throws ApiException
     */
    public function myProfileAction(ApiContext $apiContext, ObjectManager $manager)
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);
        /** @var User $user */
        $user = $this->getUser();
        $user->setSocialId($userData['network'], $userData['uid']);
        $clientExists = $apiContext->checkClientAlreadyAttached($userData['uid']);
        if ($clientExists){
            $this->addFlash(
                'warning_message',
                'Аккаунт уже привязан!'
            );
            return $this->redirectToRoute('app_show_profile');
        }
        $data = $user->__toArray();
        $apiContext->addSocialNetworkClient($data);
        $manager->persist($user);
        $manager->flush();

        $this->addFlash(
            'success_message',
            'Аккаунт успешно привязан!'
        );

        return $this->redirectToRoute('app_show_profile');
    }

    /**
     * @Route("/my-profile", name="app_show_profile")
     * @return Response
     */
    public function showMyProfileAction()
    {
        return $this->render('my_profile.html.twig');
    }

    /**
     * @Route("/sign-in", name="sign-in")
     * @param UserRepository $userRepository
     * @param Request $request
     * @param ApiContext $apiContext
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return Response
     * @throws ApiException
     */
    public function signInAction(
        UserRepository $userRepository,
        Request $request,
        ApiContext $apiContext,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $form = $this->createForm("App\Form\AuthenticationType");
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $data = $form->getData();
            $passwordEncode = $apiContext->passwordEncode($data['password']);
            $user = $userRepository->findByEmailAndPassword($data['email'], $passwordEncode);
            if ($user){
                $userHandler->makeUserSession($user);
                return $this->redirectToRoute("app_show_profile");
            }
            try {
                $client = $apiContext->checkClientCredentials($data['password'], $data['email']);
                if ($client) {
                    $user_array = $apiContext->signInClient($data['email']);
                    $user = $userHandler->createNewUser($user_array, false);
                    $manager->persist($user);
                    $manager->flush();
                    $userHandler->makeUserSession($user);
                    return $this->redirectToRoute("ping");
                } else {
                    $this->addFlash(
                        'error_message',
                        'Ты не тот, за кого себя выдаешь!'
                    );
                }
            } catch (ApiException $e) {
                $this->addFlash(
                    'error_message',
                    'Что-то где-то пошло нетак!'
                );
            }
        }
        return $this->render('sign_in.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/qwe", name="qwe")
     * @param ApiContext $apiContext
     * @return Response
     * @throws ApiException
     */
    public function qweAction(ApiContext $apiContext)
    {
        $result = $apiContext->clientExists('some & passport', '123@123.ru');
        return new Response(var_export($result, true));
    }

    /**
     * @Route("/login", name="login")
     * @param UserRepository $repository
     * @param UserHandler $userHandler
     * @param ObjectManager $manager
     * @param ApiContext $apiContext
     * @return Response
     */
    public function loginAction(
        UserRepository $repository,
        UserHandler $userHandler,
        ObjectManager $manager,
        ApiContext $apiContext)
    {
        $error = null;

        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);
        $user = $repository->findOneBySocialId($userData['uid']);
        if ($user){
            $userHandler->makeUserSession($user);
            $this->addFlash(
                'success_message',
                'Вы успешно авторизавались!'
            );
            return $this->redirectToRoute("app_show_profile");
        }
        try {
            if ($apiContext->checkClientAlreadyAttached($userData['uid'])) {
                $client = $apiContext->signInClient($userData['email']);
                if ($currentUser = $repository->findOneByPassportOrEmail($client['passport'], $client['email'])){
                    $currentUser->setVkId($client['vkId']);
                    $currentUser->setFaceBookId($client['faceBookId']);
                    $currentUser->setGoogleId($client['googleId']);
                }
                else {
                    $currentUser = $userHandler->createNewUser($client, false);
                }
                $manager->persist($currentUser);
                $manager->flush();
                $userHandler->makeUserSession($currentUser);
                return $this->redirectToRoute("app_show_profile");
            }
        } catch (ApiException $e) {
            $this->addFlash(
                'error_message',
                'Вы еще не регистрировались у нас, либо не привязали данную соц сеть к аккаунту!'
            );
        }
        return $this->redirectToRoute("sign-in");
    }
}
