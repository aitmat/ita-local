<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegisterCottageType;
use App\Form\RegisterPensionType;
use App\Form\registerObjectType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\Booking\BookingHandler;
use App\Model\Object\BookingObject;
use App\Model\Object\Cottage;
use App\Model\Object\Pension;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class objectController extends Controller
{
    const DATA_OBJECT = 'data-object';

    /**
     * @Route("/new-object", name="new-object")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newObjectAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        if (!in_array('ROLE_LANDLORD', $user->getRoles())){
            $this->addFlash(
                'warning_message',
                'У вас нет доступ для создание объекта!'
            );
            return $this->redirectToRoute('ping');
        }
        $object = new BookingObject();
        $form = $this->createForm(registerObjectType::class, $object);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            /** @var BookingObject $formData */
            $formData = $form->getData();
            /** @var User $landlord */
            $landlord = $this->getUser();
            $formData->setLandlord($landlord->getEmail());
            $this->get('session')->set(self::DATA_OBJECT, $formData);
            if ($formData->getDiscriminator() == BookingHandler::PENSION){
                return $this->redirect($this->generateUrl('create-pension'), 307);
            }
            elseif ($formData->getDiscriminator() == BookingHandler::COTTAGE){
                return $this->redirect($this->generateUrl('create-cottage'), 307);
            }
        }

        return $this->render('page/object.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/create-pension", name="create-pension")
     * @param Request $request
     * @param BookingHandler $bookingHandler
     * @param ApiContext $apiContext
     * @return \Symfony\Component\HttpFoundation\Response
     * @Method("POST")
     * @throws \App\Model\Api\ApiException
     */
    public function bookingCottageAction(Request $request, BookingHandler $bookingHandler, ApiContext $apiContext)
    {
        $pension = new Pension();
        /** @var BookingObject $mainData */
        $mainData = $this->get('session')->get(self::DATA_OBJECT);
        $pension->setAddress($mainData->getAddress());
        $form = $this->createForm(RegisterPensionType::class, $pension);
        $form->handleRequest($request);
        /** @var Pension $formData */
        $formData = $form->getData();
        if ($form->isSubmitted() && $form->isValid()){
            $pension = $bookingHandler->objectMerged($formData, $mainData);
            try {
                $apiContext->createNewObject($pension->toArray());
                $this->addFlash('success_message', 'Вы успешно создали объект!');
                return $this->redirectToRoute('ping');
            } catch (ApiException $e) {
                $this->addFlash('error_message', 'Что то где-то пошло не так или ввели не корректные данные!');
                return $this->redirectToRoute('ping');
            }
        }
        return $this->render('page/new-object.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/create-cottage", name="create-cottage")
     * @param Request $request
     * @param BookingHandler $bookingHandler
     * @param ApiContext $apiContext
     * @return \Symfony\Component\HttpFoundation\Response
     * @Method("POST")
     */
    public function bookingPensionAction(Request $request, BookingHandler $bookingHandler,  ApiContext $apiContext)
    {
        $cottage = new Cottage();
        $mainData = $this->get('session')->get(self::DATA_OBJECT);
        $cottage->setAddress($mainData->getAddress());
        $form = $this->createForm(RegisterCottageType::class, $cottage);
        $form->handleRequest($request);
        $formData = $form->getData();
        $formData->setAddress($mainData->getAddress());
        if ($form->isSubmitted() && $form->isValid()){
            $cottage = $bookingHandler->objectMerged($formData, $mainData);
            try {
                $apiContext->createNewObject($cottage->toArray());
                $this->addFlash('success_message', 'Вы успешно создали объект!');
                return $this->redirectToRoute('ping');
            } catch (ApiException $e) {
                $this->addFlash('error_message', 'Что то где-то пошло не так или ввели не корректные данные!');
                return $this->redirectToRoute('ping');
            }

        }
        return $this->render('page/new-object.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/show-more/{name}", name="app_object_more")
     * @param BookingHandler $bookingHandler
     * @param ApiContext $apiContext
     * @param string $name
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Model\Api\ApiException
     * @Method({"GET", "POST"})
     */
    public function showMoreAction(BookingHandler $bookingHandler,  ApiContext $apiContext, string $name)
    {
        $query = $apiContext->getByNameObject($name);
        $objects = $bookingHandler->arrayToObject([$query]);
        $booking = $query['booking'];
        return $this->render('page/more-about-object.html.twig', [
            'objects' => $objects,
            'booking' => $booking,
        ]);
    }

    /**
     * @Route("/object/booking/{room}/{name}", name="app_booking_room")
     * @param ApiContext $apiContext
     * @param int $room
     * @param string $name
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \App\Model\Api\ApiException
     * @Method({"GET", "POST"})
     */
    public function bookingRoomAction(ApiContext $apiContext, int $room, string $name)
    {
        $user = $this->getUser();
        if (!$user){
            $this->addFlash('error_message', 'Вам нужно авторизоваться!');
            return $this->redirectToRoute('app_object_more', ['name' => $name]);
        }
        if (in_array('ROLE_LANDLORD', $user->getRoles())){
            $this->addFlash('error_message', 'У вас нет доступ для бронирование!');
            return $this->redirectToRoute('app_object_more', ['name' => $name]);
        }
        $data = [
            'name' => $name,
            'room' => $room,
            'tenant' => $user->getEmail()
        ];
        $res = $apiContext->bookingRoomObject($data);

        if ($res){
            $this->addFlash('success_message', 'Вы успешно бронировали!');
            return $this->redirectToRoute('app_object_more', ['name' => $name]);
        }
        return $this->redirectToRoute('ping');
    }
}
