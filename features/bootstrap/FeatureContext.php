<?php

use Behat\Mink\Exception\ElementNotFoundException;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends AttractorContext
{
    /**
     * @When /^я вижу слово "([^"]*)" где\-то на странице$/
     * @throws \Behat\Mink\Exception\ResponseTextException
     */
    public function яВижуСловоГдеТоНаСтранице($arg1)
    {
        $this->assertPageContainsText($arg1);
    }

    /**
     * @When /^я нахожусь на главной странице$/
     */
    public function яНахожусьНаГлавнойСтранице()
    {
        $this->visit($this->getContainer()->get('router')->generate('ping'));
    }

    /**
     * @When /^я вижу ссылку "([^"]*)" и нажимаю на нее$/
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function яВижуСсылкуИНажимаюНаНее($link) {
        $this->clickLink($link);
    }

    /**
     * @When /^я нахожусь на странице авторизации/
     */
    public function яНахожусьНаСтраницеАвторизации()
    {
        $this->visit($this->getContainer()->get('router')->generate('sign-in'));
    }

    /**
     * @When /^ввожу данные в поля "([^"]*)" и "([^"]*)",потом кликаю на кнопку "([^"]*)"$/
     * @param $field1
     * @param $field2
     * @param $button
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function яВижуФормуАвторизацииИАвторизуюсьКакАрендатор($field1,$field2,$button) {
        $this->fillField($field1, "123@123.ru");
        $this->fillField($field2, "pass123");
        $this->pressButton($button);
    }

    /**
     * @When /^ввожу данные арендодателя в поля "([^"]*)" и "([^"]*)",потом кликаю на кнопку "([^"]*)"$/
     * @param $field1
     * @param $field2
     * @param $button
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function яВижуФормуАвторизацииИАвторизуюсьКакАрендодатель($field1,$field2,$button) {
        $this->fillField($field1, "111@111.ru");
        $this->fillField($field2, "pass111");
        $this->pressButton($button);
    }

    /**
     * @When /^если я пытаюсь создать объект "([^"]*)"$/
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function еслиЯПытаюсьСоздатьОбъект($link)
    {
        $this->clickLink($link);
    }

    /**
     * @When /^увижу сообщене про доступа "([^"]*)"$/
     * @throws \Behat\Mink\Exception\ResponseTextException
     */
    public function увижуСообщенеПроДоступа($arg1)
    {
        $this->assertPageContainsText($arg1);
    }

    /**
     * @When /^заполняю полей и нажимаю далее "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)"$/
     * @param $locator
     * @param $field1
     * @param $field2
     * @param $field3
     * @param $field4
     * @param $field6
     * @param $button
     * @throws ElementNotFoundException
     */
    public function заполняюПолейИнажимаюДалее($locator, $field1, $field2, $field3, $field4, $field6, $button)
    {
        $field = $this->getSession()->getPage()->findField($locator);
        $field->selectOption($locator);
        $this->fillField($field1, "Astoria Village Issyk Kul");
        $this->fillField($field2, "10");
        $this->fillField($field3, "Хозяин");
        $this->fillField($field4, "+996777777777");
        $this->getSession()->evaluateScript("$('#register_object_address').val('42.685701, 77.240226');");
        $this->fillField($field6, "1000");
        $this->pressButton($button);
    }

    /**
     * @When /^выбираю критерии объекта "([^"]*)"$/
     * @param $arg1
     * @throws ElementNotFoundException
     */
    public function выбираюКритерииОбъекта($arg1)
    {
        $this->checkOption($arg1);
    }

    /**
     * @When /^нажимаю на кнопку "([^"]*)"$/
     * @param $arg1
     * @throws ElementNotFoundException
     */
    public function нажимаюНаКнопку($arg1)
    {
        $this->pressButton($arg1);
    }
}